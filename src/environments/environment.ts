// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDqbYe-1F6dth_U-gQxuUWrzkY0ZMyzaKs",
  authDomain: "authenticationdb-546a3.firebaseapp.com",
  databaseURL: "https://authenticationdb-546a3.firebaseio.com",
  projectId: "authenticationdb-546a3",
  storageBucket: "authenticationdb-546a3.appspot.com",
  messagingSenderId: "195192695912",
  appId: "1:195192695912:web:23c73283785f51b5"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
